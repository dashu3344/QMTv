package com.yztc.bean;

/**
 * Created by My on 2016/11/8.
 */

public class Biaoti {


    /**
     * id : 0
     * name : 精彩推荐
     * is_default : 0
     * slug :
     * type : 1
     * screen : 0
     */

    private int id;
    private String name;
    private int is_default;
    private String slug;
    private int type;
    private int screen;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIs_default() {
        return is_default;
    }

    public void setIs_default(int is_default) {
        this.is_default = is_default;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getScreen() {
        return screen;
    }

    public void setScreen(int screen) {
        this.screen = screen;
    }
}
