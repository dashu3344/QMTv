package com.yztc.bean;

/**
 * Created by lxs on 2016/11/7.
 */

public class Constans {
    // 数据为空
    public static final int NULL = 0;
    //   网络错误
    public static final int ERRO = 1;
    //网络正常
    public static final int OK = 2;

    //全名星秀地址 不需要加载分页
    public static final String QUAN_MIN_XING_XIU = "http://www.quanmin.tv/json/categories/beauty/list.json?09050021&uid=QO8PE69STWW4G6WO&screen=2&os=1&v=2.1.3&device=865168025985085&ch=OT_wdjcpd&ver=4&sh=1280&net=0&sw=720";
    //炉石传说地址
    public static final String LU_SHI_CHUAN_SHUO = "http://www.quanmin.tv/json/categories/heartstone/list.json?09050026&uid=QO8PE69STWW4G6WO&screen=2&os=1&v=2.1.3&device=865168025985085&ch=OT_wdjcpd&ver=4&sh=1280&net=0&sw=720";
    //守望先锋
    public static final String SHOU_WANG_XIAN_FENG = "http://www.quanmin.tv/json/categories/overwatch/list.json?09050032&uid=QO8PE69STWW4G6WO&screen=2&os=1&v=2.1.3&device=865168025985085&ch=OT_wdjcpd&ver=4&sh=1280&net=0&sw=720";
    //英雄联盟
    public static final String YING_XIONG_LIAN_MENG = "http://www.quanmin.tv/json/categories/lol/list.json?uid=QO8PE69STWW4G6WO&screen=2&os=1&v=2.1.3&device=865168025985085&ch=OT_wdjcpd&ver=4&sh=1280&net=0&sw=720";
    //二次元
    public static final String ER_CI_YUAN = "http://www.quanmin.tv/json/categories/erciyuan/list.json?09050035&uid=QO8PE69STWW4G6WO&screen=2&os=1&v=2.1.3&device=865168025985085&ch=OT_wdjcpd&ver=4&sh=1280&net=0&sw=720";
    //全部
    public static final String QUAN_BU = "http://www.quanmin.tv/json/play/list.json?11081714&os=1&v=2.2&ver=4";
    //推荐
    public static final String TUI_JIAN = "http://www.quanmin.tv/json/app/index/recommend/list-android.json?11081714&os=1&v=2.2&ver=4";
    //颜值控
    public static final String YAN_ZHI="http://www.quanmin.tv/json/categories/love/list.json?11081707&os=1&v=2.2&ver=4";
    //viewPger
    public static final String TJ_VIEW_PAGER="http://www.quanmin.tv/json/page/app-data/info.json";
}

