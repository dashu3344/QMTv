package com.yztc.bean;

import java.util.List;

/**
 * Created by lxs on 2016/11/8.
 */

public class YxlmBean {

    /**
     * nick : 孙悟空zzzz
     * video : http://thumb.quanmin.tv/333.mp4?t=1478574900
     * screen : 0
     * grade :
     * start_time : 2016-11-08 10:13:05
     * avatar : http://image.quanmin.tv/avatar/2ef14b476e146062065af2857d953be0?imageView2/2/w/300/
     * status : 1
     * thumb : http://snap.quanmin.tv/333-1478575081-926.jpg?imageView2/2/w/390/
     * intro : 新浪微博：孙悟空kingking百度贴吧：lol猴王悟空
     * level : 0
     * recommend_image : http://image.quanmin.tv/94c7901bce10d0636e704bba43a01045jpg
     * like : 16
     * uid : 333
     * view : 539556
     * love_cover :
     * category_name : 英雄联盟
     * announcement : 每天早上10点直播
     * create_at : 2016-11-08 10:13:05
     * play_at : 2016-11-08 10:13:05
     * video_quality : 234
     * default_image :
     * category_id : 1
     * follow : 554266
     * beauty_cover :
     * slug :
     * title : 国服最强猴王
     * category_slug : lol
     * app_shuffling_image : http://image.quanmin.tv/d6808580cd0c897366c76df33ed95902jpg
     * weight : 145263060
     * hidden : false
     */

    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        private String nick;
        private String video;
        private int screen;
        private String grade;
        private String start_time;
        private String avatar;
        private String status;
        private String thumb;
        private String intro;
        private String level;
        private String recommend_image;
        private String like;
        private String uid;
        private String view;
        private String love_cover;
        private String category_name;
        private String announcement;
        private String create_at;
        private String play_at;
        private String video_quality;
        private String default_image;
        private String category_id;
        private int follow;
        private String beauty_cover;
        private String slug;
        private String title;
        private String category_slug;
        private String app_shuffling_image;
        private String weight;
        private boolean hidden;

        public String getNick() {
            return nick;
        }

        public void setNick(String nick) {
            this.nick = nick;
        }

        public String getVideo() {
            return video;
        }

        public void setVideo(String video) {
            this.video = video;
        }

        public int getScreen() {
            return screen;
        }

        public void setScreen(int screen) {
            this.screen = screen;
        }

        public String getGrade() {
            return grade;
        }

        public void setGrade(String grade) {
            this.grade = grade;
        }

        public String getStart_time() {
            return start_time;
        }

        public void setStart_time(String start_time) {
            this.start_time = start_time;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getThumb() {
            return thumb;
        }

        public void setThumb(String thumb) {
            this.thumb = thumb;
        }

        public String getIntro() {
            return intro;
        }

        public void setIntro(String intro) {
            this.intro = intro;
        }

        public String getLevel() {
            return level;
        }

        public void setLevel(String level) {
            this.level = level;
        }

        public String getRecommend_image() {
            return recommend_image;
        }

        public void setRecommend_image(String recommend_image) {
            this.recommend_image = recommend_image;
        }

        public String getLike() {
            return like;
        }

        public void setLike(String like) {
            this.like = like;
        }

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public String getView() {
            return view;
        }

        public void setView(String view) {
            this.view = view;
        }

        public String getLove_cover() {
            return love_cover;
        }

        public void setLove_cover(String love_cover) {
            this.love_cover = love_cover;
        }

        public String getCategory_name() {
            return category_name;
        }

        public void setCategory_name(String category_name) {
            this.category_name = category_name;
        }

        public String getAnnouncement() {
            return announcement;
        }

        public void setAnnouncement(String announcement) {
            this.announcement = announcement;
        }

        public String getCreate_at() {
            return create_at;
        }

        public void setCreate_at(String create_at) {
            this.create_at = create_at;
        }

        public String getPlay_at() {
            return play_at;
        }

        public void setPlay_at(String play_at) {
            this.play_at = play_at;
        }

        public String getVideo_quality() {
            return video_quality;
        }

        public void setVideo_quality(String video_quality) {
            this.video_quality = video_quality;
        }

        public String getDefault_image() {
            return default_image;
        }

        public void setDefault_image(String default_image) {
            this.default_image = default_image;
        }

        public String getCategory_id() {
            return category_id;
        }

        public void setCategory_id(String category_id) {
            this.category_id = category_id;
        }

        public int getFollow() {
            return follow;
        }

        public void setFollow(int follow) {
            this.follow = follow;
        }

        public String getBeauty_cover() {
            return beauty_cover;
        }

        public void setBeauty_cover(String beauty_cover) {
            this.beauty_cover = beauty_cover;
        }

        public String getSlug() {
            return slug;
        }

        public void setSlug(String slug) {
            this.slug = slug;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getCategory_slug() {
            return category_slug;
        }

        public void setCategory_slug(String category_slug) {
            this.category_slug = category_slug;
        }

        public String getApp_shuffling_image() {
            return app_shuffling_image;
        }

        public void setApp_shuffling_image(String app_shuffling_image) {
            this.app_shuffling_image = app_shuffling_image;
        }

        public String getWeight() {
            return weight;
        }

        public void setWeight(String weight) {
            this.weight = weight;
        }

        public boolean isHidden() {
            return hidden;
        }

        public void setHidden(boolean hidden) {
            this.hidden = hidden;
        }
    }
}
