package com.yztc.utils;

import com.alibaba.fastjson.JSON;

import com.yztc.bean.UserRoom;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by Administrator on 2016/11/8.
 */

public class ParseUserRoom {
    public static List<UserRoom> fromStringToList(String jsonString) {
        List<UserRoom> lists = null;
        try {
            JSONObject obj = new JSONObject(jsonString);
            JSONArray array = obj.getJSONArray("data");
            lists = JSON.parseArray(array.toString(), UserRoom.class);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return lists;
    }
}