package com.yztc.utils;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;

/**
 * Created by My on 2016/11/8.
 */

public class okHttpUtils {
    private okHttpUtils() {
    }

    private static okHttpUtils OK = new okHttpUtils();

    //静态工厂方法
    public static okHttpUtils getInstance() {
        return OK;
    }

    OkHttpClient client = new OkHttpClient();

    public Call run(String url) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .build();
        Call call=client.newCall(request);
        return call;
    }
}
