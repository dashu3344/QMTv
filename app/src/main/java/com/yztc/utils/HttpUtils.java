package com.yztc.utils;




import java.io.IOException;
import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

//Created by My on 2016/10/8.
 
public class HttpUtils {
    //单例只产生一个httputils对象
    private HttpUtils() {
    }
    public static HttpUtils httpUtils = new HttpUtils();

    public static HttpUtils getHttpUtils() {
        return httpUtils;
    }

    static OkHttpClient okHttpClient = new OkHttpClient();
    //直接拿到返回的json
    public static byte[] getJson(String url) throws IOException {
        Request request = new Request.Builder().url(url).build();
        Call call = okHttpClient.newCall(request);

        Response response = call.execute();
        //response.body().
        return response.body().bytes();
    }

}
