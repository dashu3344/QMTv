package com.yztc.utils;


import android.util.Log;

import com.alibaba.fastjson.JSON;

import com.yztc.bean.ListBean;
import com.yztc.bean.RoomBean;
import com.yztc.bean.TJViewPagerBean;
import com.yztc.bean.YxlmBean;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by lxs on 2016/11/7.
 */

public class JsonUtils {

    private List<RoomBean> roomBeen;

    //解析英雄联盟
    public static List<YxlmBean.DataBean> froJsonToList(String jsonString) {
        YxlmBean yxlm = new YxlmBean();
        List<YxlmBean.DataBean> list = yxlm.getData();
        try {
            JSONObject obj = new JSONObject(jsonString);
            JSONArray data = obj.getJSONArray("data");

            list = JSON.parseArray(data.toString(), YxlmBean.DataBean.class);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return list;
    }

    //解析推荐
    public static List<RoomBean> parseJson(String jsonString) {

        try {

            JSONObject obj = new JSONObject(jsonString);

            JSONArray array = obj.optJSONArray("room");

            List<RoomBean> roomBeen =  JSON.parseArray(array.toString(), RoomBean.class);

            for (int i = 0; i < array.length(); i++) {


                List<ListBean> listBeen = new ArrayList<>();

                JSONObject obj3 = array.optJSONObject(i);

                JSONArray array1 = obj3.optJSONArray("list");

                List<ListBean> listBeen1 = JSON.parseArray(array1.toString(), ListBean.class);

                listBeen.addAll(listBeen1);
                roomBeen.get(i).setListBeen(listBeen);

            }
            return roomBeen;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    //解析 推荐里面的ViewPagershuj
    public static List<TJViewPagerBean> fromTJToList(String jsonString){
        List<TJViewPagerBean> tjViewPagerBeen = new ArrayList<>();
        try {
            JSONObject obj = new JSONObject(jsonString);

            JSONArray array = obj.optJSONArray("app-focus");

            for (int i = 0; i < array.length(); i++) {
                JSONObject obj3 = array.optJSONObject(i);
                JSONObject obj4 = obj3.optJSONObject("link_object");

                TJViewPagerBean pagerBean = JSON.parseObject(obj4.toString(), TJViewPagerBean.class);
                tjViewPagerBeen.add(pagerBean);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return tjViewPagerBeen;
    }
}
