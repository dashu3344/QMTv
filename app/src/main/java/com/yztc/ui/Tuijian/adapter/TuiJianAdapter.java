package com.yztc.ui.Tuijian.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

/**
 * Created by lxs on 2016/11/7.
 */

public class TuiJianAdapter extends FragmentPagerAdapter {
    private List<Fragment> list;
    private String[] titles;

    public TuiJianAdapter(FragmentManager fm,List<Fragment> list,String[] titles) {
        super(fm);
        this.list = list;
        this.titles = titles;
    }

    @Override
    public Fragment getItem(int position) {
        return list.get(position);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {

        return titles[position];
    }
}
