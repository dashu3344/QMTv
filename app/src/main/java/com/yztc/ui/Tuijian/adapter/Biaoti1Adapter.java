package com.yztc.ui.Tuijian.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.yztc.qmtv.R;

import java.util.List;

/**
 * Created by My on 2016/11/10.
 */

public class Biaoti1Adapter extends BaseAdapter {
    private Context context;
    private List<String> list;

    public Biaoti1Adapter( List<String> list,Context context) {
        this.context = context;
        this.list = list;
    }



    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    BiaotiAdapter.ViewHolder holder = null;
    @SuppressLint("WrongViewCast")
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        if(view==null){
            holder = new BiaotiAdapter.ViewHolder();
            view = View.inflate(context, R.layout.bt_gv2,null);
            holder.button = (TextView) view.findViewById(R.id.bt_gv2_btn);



            view.setTag(holder);
        }else {
            holder = (BiaotiAdapter.ViewHolder) view.getTag();

        }

        holder.button.setText(list.get(i).toString());
        return view;
    }

    static class ViewHolder {
        TextView button;
    }
}
