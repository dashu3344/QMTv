package com.yztc.ui.Tuijian.fragment;


import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshGridView;
import com.yztc.bean.Constans;
import com.yztc.bean.YxlmBean;
import com.yztc.qmtv.R;
import com.yztc.ui.Tuijian.adapter.YanZhiAdapter;
import com.yztc.utils.HttpHelper;
import com.yztc.utils.HttpUtils;
import com.yztc.utils.JsonUtils;
import com.yztc.widget.LoadingView;

import java.io.IOException;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class yanZhiFragment extends Fragment {


    private PullToRefreshGridView mPullGridView;
    private GridView mGv;
    private LoadingView loadingView;

    public yanZhiFragment() {
        // Required empty public constructor
    }

    private Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case Constans.ERRO:
                    break;
                case Constans.NULL:
                    break;
                case Constans.OK:
                    List<YxlmBean.DataBean> list = (List<YxlmBean.DataBean>) msg.obj;
                    YanZhiAdapter adapter = new YanZhiAdapter(getActivity(),list);
                    mGv.setAdapter(adapter);
                    loadingView.setVisibility(View.GONE);
                    mPullGridView.setVisibility(View.VISIBLE);
                    break;
            }
        }
    };
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_yan_zhi, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //刷新 gridView
        mPullGridView = (PullToRefreshGridView) view.findViewById(R.id.yanzhi_pull_refresh_grid);
        //设置刷新模式
        mPullGridView.setMode(PullToRefreshBase.Mode.PULL_FROM_START);
        //关联
        mGv = mPullGridView.getRefreshableView();
        //设置刷新监听事件
        mPullGridView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<GridView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<GridView> refreshView) {
                //下拉刷新更新数据
                mPullGridView.setVisibility(View.VISIBLE);
                mPullGridView.onRefreshComplete();
            }
            @Override
            public void onPullUpToRefresh(PullToRefreshBase<GridView> refreshView) {
                //上拉刷新加载下一页
                mPullGridView.onRefreshComplete();
            }
        });
        mPullGridView.setVisibility(View.GONE);
        dongHua(view);
        //加载数据
        getHttpResult();
    }

     void getHttpResult(){
         Bundle bundle =getArguments();
         final String url = bundle.getString("url");
         new Thread(new Runnable() {
             @Override
             public void run() {
                 if(!TextUtils.isEmpty(url)){
                     Message msg = Message.obtain();
                   if(HttpHelper.isNetworkConnected(getActivity())){
                       try {
                           byte[] json = HttpUtils.getHttpUtils().getJson(url);
                           List<YxlmBean.DataBean> list = JsonUtils.froJsonToList(new String(json, 0, json.length));
//                           Log.i("tag","--------list-------颜值控-----------"+list.size());

                           if(list.size()!=0&&list!=null){
                               msg.what=Constans.OK;
                               msg.obj = list;
                               handler.sendMessageDelayed(msg,3000);
                           }else {
                                msg.what = Constans.NULL;
                           }
                       } catch (IOException e) {
                           e.printStackTrace();
                       }
                   }else {
                       msg.what = Constans.ERRO;
                   }
                 }else {
                     Log.i("tag","颜值控-------------地址空");
                 }

             }
         }).start();
     }

    //加载动画
    void dongHua(View view){
        loadingView = (LoadingView) view.findViewById(R.id.loadingView);
        ImageView imageView = (ImageView) loadingView.findViewById(R.id.loadingIv);
        imageView.setImageResource(R.drawable.loading_anim);
        AnimationDrawable animationDrawable = (AnimationDrawable) imageView.getDrawable();
        animationDrawable.start();
    }
}
