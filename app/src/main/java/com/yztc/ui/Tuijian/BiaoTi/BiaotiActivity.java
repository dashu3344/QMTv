package com.yztc.ui.Tuijian.BiaoTi;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.yztc.bean.Biaoti;
import com.yztc.qmtv.LanmuActivity;
import com.yztc.qmtv.R;
import com.yztc.ui.Tuijian.adapter.Biaoti1Adapter;
import com.yztc.ui.Tuijian.adapter.BiaotiAdapter;
import com.yztc.utils.okHttpUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class BiaotiActivity extends Activity {
    private GridView bt_gv1;
    private GridView bt_gv2;
    private ImageView bt_image;
    private TextView bt_tv;
    private TextView bt_tv_chang;
    private TextView bt_tv_quan;
    private List<Biaoti> list;
    private String path1;
    private String url= "http://www.quanmin.tv/json/app/index/category/info-android.json?os=1&v=2.2&ver=4";

    private BiaotiAdapter adapter1;
    private Biaoti1Adapter adapter2;


    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            list = (List<Biaoti>) msg.obj;
            List<String> list1=new ArrayList<>();
            List<String> list2=new ArrayList<>();
            for (int i = 0; i <list.size() ; i++) {
                String slug=list.get(i).getSlug();
                int is=list.get(i).getIs_default();

                if (slug!=""&&is==1){
                    String name=list.get(i).getName();
                    list1.add(name);
                    }else if(slug!=""&&is==0){
                    String name2=list.get(i).getName();
                    list2.add(name2);

                    }
                }
            adapter1 = new BiaotiAdapter(list1, BiaotiActivity.this);
            bt_gv1.setAdapter(adapter1);
            adapter2 = new Biaoti1Adapter(list2, BiaotiActivity.this);
            bt_gv2.setAdapter(adapter2);
            adapter1.notifyDataSetChanged();
            adapter2.notifyDataSetChanged();

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_biaoti);

        //初始化
        initView();

        getList();

        //点击按钮返回上一页
        bt_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BiaotiActivity.this.finish();
            }
        });

        bt_gv1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String slug = list.get(i).getSlug();
                path1 =  "http://www.quanmin.tv/json/categories/"+slug+"/list.json?uid=QO8PE69STWW4G6WO&screen=2&os=1&v=2.1.3&device=865168025985085&ch=OT_wdjcpd&ver=4&sh=1280&net=0&sw=720 ";
                Intent intent = new Intent(BiaotiActivity.this, LanmuActivity.class);
                intent.putExtra("path1",path1);
                startActivity(intent);
            }
        });

        //管理按钮点击事件
        bt_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String ss = bt_tv.getText().toString();
                if ("管理".equals(ss)) {

                    bt_tv.setText("完成");
                    bt_tv.setTextColor(Color.RED);
                    bt_tv_chang.setText("长按频道拖拽排序，点击删除");
                    bt_tv_chang.setTextColor(Color.GRAY);
                    bt_tv_quan.setText("点击添加为常用频道");
                    bt_tv_quan.setTextColor(Color.GRAY);
                } else if ("完成".equals(ss)) {
                    bt_tv.setText("管理");
                    bt_tv.setTextColor(Color.GRAY);
                    bt_tv_chang.setText("");
                    bt_tv_quan.setText("");
                }
            }
        });
    }

    //初始化
    public void initView() {
        bt_gv1 = (GridView) findViewById(R.id.bt_gv1);
        bt_gv2 = (GridView) findViewById(R.id.bt_gv2);
        bt_image = (ImageView) findViewById(R.id.bt_image);
        bt_tv = (TextView) findViewById(R.id.bt_tv);
        bt_tv_chang = (TextView) findViewById(R.id.bt_tv_chang);
        bt_tv_quan = (TextView) findViewById(R.id.bt_tv_quan);
    }

    //下载数据
    public void getList() {
        final Message msg = Message.obtain();
        try {
            Call call = okHttpUtils.getInstance().run(url);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    Looper.prepare();
                    Toast.makeText(BiaotiActivity.this, "数据获取失败", Toast.LENGTH_SHORT).show();
                    Looper.loop();
                }


                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    String jsonString = response.body().string();
                    // Log.i("aaa","---"+jsonString);
                    List<Biaoti> list = JSON.parseArray(jsonString, Biaoti.class);

//                    for (int i = 0; i <list.size() ; i++) {
//                       String slug= list.get(i).getSlug();
//                        int is=list.get(i).getIs_default();
//                        if (slug!=""||is==1){
//                        String list1=list.get(i).getName();
//                            msg.what
//                        }
//                    }

                    msg.obj = list;
                    // Log.i("aaa",list+"");
                    handler.sendMessage(msg);
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
