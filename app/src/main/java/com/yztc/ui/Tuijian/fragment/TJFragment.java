package com.yztc.ui.Tuijian.fragment;


import android.app.Activity;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshGridView;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.yztc.bean.Constans;

import com.yztc.bean.ListBean;
import com.yztc.bean.RoomBean;
import com.yztc.bean.TJViewPagerBean;
import com.yztc.qmtv.R;
import com.yztc.ui.Tuijian.adapter.HeadViewPagerAdapter;
import com.yztc.ui.Tuijian.adapter.TJListViewAdapter;
import com.yztc.utils.HttpHelper;
import com.yztc.utils.HttpUtils;
import com.yztc.utils.JsonUtils;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * 标题的 推荐fragment
 */
public class TJFragment extends Fragment  {

    private final int BEAN = 4;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case Constans.ERRO:
                    break;
                case Constans.NULL:
                    break;
                case Constans.OK:
                    List<RoomBean> roomBeen = (List<RoomBean>) msg.obj;
                    TJListViewAdapter adapter = new TJListViewAdapter(getActivity(), roomBeen);
                    //添加到适配器中
                    mLv.setAdapter(adapter);

                    tj_iv.setVisibility(View.GONE);
                    mPullListView.setVisibility(View.VISIBLE);
                    break;
            }
        }
    };

    private PullToRefreshListView mPullListView;
    private ListView mLv;
    private List<TJViewPagerBean> bean;
    private RelativeLayout mLinearLayout;
    private Activity mActivity;
    private AnimationDrawable animationDrawable;
    private ImageView tj_iv;

    public TJFragment() {

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_tj, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mPullListView = (PullToRefreshListView) view.findViewById(R.id.tj_pull_listView);

        //
        mPullListView.setMode(PullToRefreshBase.Mode.BOTH);

        mLv = mPullListView.getRefreshableView();
        //刷新监听
        mPullListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {

                mPullListView.onRefreshComplete();

            }
            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                mPullListView.onRefreshComplete();
            }
        });
        //碎片父布局
        mLinearLayout = (RelativeLayout) view.findViewById(R.id.tj_fragment_layout);

        tj_iv = (ImageView) view.findViewById(R.id.imageView);
        tj_iv.setImageResource(R.drawable.loading_anim);
        animationDrawable = (AnimationDrawable) tj_iv.getDrawable();
        animationDrawable.start();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        mPullListView.setVisibility(View.GONE);
        getHttpResult();

    }

    //开启线程下载数据
    public void getHttpResult() {
        Bundle bundle = getArguments();
        final String url = bundle.getString("url");

        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = Message.obtain();
                if (HttpHelper.isNetworkConnected(getActivity())) {

                    try {
                        //下载 GridView 数据
                        byte[] json = HttpUtils.getHttpUtils().getJson(url);
                        //解析数据
                        List<RoomBean> roomBeen = JsonUtils.parseJson(new String(json));

                        msg.what = Constans.OK;
                        msg.obj = roomBeen;
                        handler.sendMessage(msg);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.i("tag", "推荐----------" + "网络异常........");
                    msg.what = Constans.ERRO;
                }
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                //ViewPager数据
                Message msg = Message.obtain();
                byte[] vp = new byte[0];
                try {
                    vp = HttpUtils.getHttpUtils().getJson(Constans.TJ_VIEW_PAGER);
                    List<TJViewPagerBean> been = JsonUtils.fromTJToList(new String(vp));

                    msg.what = BEAN;
                    msg.obj = been;
                    handler.sendMessageDelayed(msg,3000);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }).start();
    }

}
