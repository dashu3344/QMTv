package com.yztc.ui.Tuijian;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.yztc.bean.Constans;
import com.yztc.qmtv.R;
import com.yztc.ui.Tuijian.BiaoTi.BiaotiActivity;
import com.yztc.ui.Tuijian.adapter.TuiJianAdapter;
import com.yztc.ui.Tuijian.fragment.QuanBuFragment;
import com.yztc.ui.Tuijian.fragment.TJFragment;
import com.yztc.ui.Tuijian.fragment.yanZhiFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class TuiJianFragment extends Fragment implements View.OnClickListener{


    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    private List<Fragment> list;
    private String[] titles;
    private ImageView tuijian_image;
    private LinearLayout mLinearLayout;

    public TuiJianFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //获取标题
        titles = getResources().getStringArray(R.array.tuijian_title_array);
        return inflater.inflate(R.layout.fragment_tui_jian, container, false);


    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        mTabLayout = (TabLayout) view.findViewById(R.id.tuijian_tabLayout);
        mViewPager = (ViewPager) view.findViewById(R.id.tuijian_viewPager);
        tuijian_image= (ImageView) view.findViewById(R.id.tuijian_image);
        tuijian_image.setOnClickListener(this);
        //关联在一起
        mTabLayout.setupWithViewPager(mViewPager,true);
        initFragment();

        TuiJianAdapter adapter = new TuiJianAdapter(getFragmentManager(),list,titles);

        mViewPager.setAdapter(adapter);

//        ViewPager mViewPager= (ViewPager) view.findViewById(R.id.tj_viewPager);

//        mLinearLayout = (LinearLayout) view.findViewById(R.id.tuijian_fragment);

//        mLinearLayout.setVisibility(View.GONE);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tuijian_image:
                Intent intent=new Intent(getActivity(),BiaotiActivity.class);
                startActivity(intent);
                break;

        }
    }

    //初始化viewPager里面的Fragment
    public void initFragment(){
        list = new ArrayList<>();
        //全部  英雄联盟   全民星秀 炉石传说 守望先锋 二次元 布局一样 为GridView  只需改变网址 将网址传过去
        // 颜值控 为另一布局
        TJFragment tj = new TJFragment();
        Bundle bundle = new Bundle();
        bundle.putString("url",Constans.TUI_JIAN);
        tj.setArguments(bundle);
        QuanBuFragment quanbu = new QuanBuFragment();
        bundle = new Bundle();
        bundle.putString("url",Constans.QUAN_BU);
        quanbu.setArguments(bundle);
        //英雄联盟
        QuanBuFragment yxlm = new QuanBuFragment();
        bundle = new Bundle();
        bundle.putString("url",Constans.YING_XIONG_LIAN_MENG);
        yxlm.setArguments(bundle);
        //全民星秀
        QuanBuFragment qmxx = new QuanBuFragment();
        bundle = new Bundle();
        bundle.putString("url", Constans.QUAN_MIN_XING_XIU);
        qmxx.setArguments(bundle);
        //炉石传说
        QuanBuFragment lscs = new QuanBuFragment();
        bundle = new Bundle();
        bundle.putString("url",Constans.LU_SHI_CHUAN_SHUO);
        lscs.setArguments(bundle);
        //守望先锋
        QuanBuFragment swxf = new QuanBuFragment();
        bundle = new Bundle();
        bundle.putString("url",Constans.SHOU_WANG_XIAN_FENG);
        swxf.setArguments(bundle);
        //二次元
        QuanBuFragment ecy = new QuanBuFragment();
        bundle = new Bundle();
        bundle.putString("url",Constans.ER_CI_YUAN);
        ecy.setArguments(bundle);
        //颜值控
        yanZhiFragment yz = new yanZhiFragment();
        bundle = new Bundle();
        bundle.putString("url",Constans.YAN_ZHI);
        yz.setArguments(bundle);
        list.add(tj);
        list.add(quanbu);
        list.add(yxlm);
        list.add(qmxx);
        list.add(lscs);
        list.add(yz);
        list.add(swxf);
        list.add(ecy);

    }
}
