package com.yztc.ui.Tuijian.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.yztc.bean.YxlmBean;
import com.yztc.qmtv.R;
import com.yztc.widget.GlideCircleTransform;

import java.util.List;

/**
 * Created by lxs on 2016/11/8.
 */

public class QuanBuAdapter extends BaseAdapter {
    private Context context;
    private List<YxlmBean.DataBean> data;

    public QuanBuAdapter(Context context, List<YxlmBean.DataBean> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    ViewHolder holder = null;
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        if(view==null){
            holder = new ViewHolder();
            view = View.inflate(context, R.layout.quanbu_gridview_item,null);
            holder.ivThumb = (ImageView) view.findViewById(R.id.quanbu_iv_live);
            holder.ivAvatar = (ImageView) view.findViewById(R.id.quanbu_iv_icon);

            holder.tvNick = (TextView) view.findViewById(R.id.quanbu_tv_zhubo);
            holder.tvTitle = (TextView) view.findViewById(R.id.quanbu_tv_title);
            holder.tvView = (TextView) view.findViewById(R.id.quanbu_view_tv);
            view.setTag(holder);
        }else {
            holder = (ViewHolder) view.getTag();

        }

        Glide.with(context).load(data.get(i).getThumb()).placeholder(R.mipmap.live_default).into(holder.ivThumb);
        Glide.with(context).load(data.get(i).getAvatar()).transform(new GlideCircleTransform(context)).override(80,80).placeholder(R.mipmap.img_touxiang_default).into(holder.ivAvatar);
        holder.tvTitle.setText(data.get(i).getTitle());
        holder.tvNick.setText(data.get(i).getNick());

        int bofang = Integer.parseInt(data.get(i).getView());
        if(bofang>10000){
            double bofang2 = bofang / 10000.0;
            java.text.DecimalFormat   df   =new   java.text.DecimalFormat("#.00");
            String format = df.format(bofang2);
            holder.tvView.setText("#"+format+"万");
        }else {
            holder.tvView.setText("#"+bofang);
        }



        return view;
    }

    static class ViewHolder {
        ImageView ivThumb,ivAvatar;
        TextView tvNick,tvTitle,tvView;
    }
}
