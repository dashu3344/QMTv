package com.yztc.ui.Lanmu.Been_Lanmu;

/**
 * Created by Administrator on 2016/11/9.
 */

public class Lanmu_Class_Data {
    private String recommend_image;

    private String announcement;

    private String title;



    private String intro;

    private String video;

    private int screen;

    private String push_ip;

    private String love_cover;

    private String category_id;

    private String video_quality;

    private String like;

    private String default_image;

    private String slug;

    private String weight;

    private String status;

    private String level;

    private String avatar;

    private String uid;



    private String view;

    private String category_slug;

    private String nick;

    private String beauty_cover;

    private String app_shuffling_image;

    private int follow;



    private String category_name;

    private String thumb;

    private String grade;

    private boolean hidden;

    public void setRecommend_image(String recommend_image){
        this.recommend_image = recommend_image;
    }
    public String getRecommend_image(){
        return this.recommend_image;
    }
    public void setAnnouncement(String announcement){
        this.announcement = announcement;
    }
    public String getAnnouncement(){
        return this.announcement;
    }
    public void setTitle(String title){
        this.title = title;
    }
    public String getTitle(){
        return this.title;
    }

    public void setIntro(String intro){
        this.intro = intro;
    }
    public String getIntro(){
        return this.intro;
    }
    public void setVideo(String video){
        this.video = video;
    }
    public String getVideo(){
        return this.video;
    }
    public void setScreen(int screen){
        this.screen = screen;
    }
    public int getScreen(){
        return this.screen;
    }
    public void setPush_ip(String push_ip){
        this.push_ip = push_ip;
    }
    public String getPush_ip(){
        return this.push_ip;
    }
    public void setLove_cover(String love_cover){
        this.love_cover = love_cover;
    }
    public String getLove_cover(){
        return this.love_cover;
    }
    public void setCategory_id(String category_id){
        this.category_id = category_id;
    }
    public String getCategory_id(){
        return this.category_id;
    }
    public void setVideo_quality(String video_quality){
        this.video_quality = video_quality;
    }
    public String getVideo_quality(){
        return this.video_quality;
    }
    public void setLike(String like){
        this.like = like;
    }
    public String getLike(){
        return this.like;
    }
    public void setDefault_image(String default_image){
        this.default_image = default_image;
    }
    public String getDefault_image(){
        return this.default_image;
    }
    public void setSlug(String slug){
        this.slug = slug;
    }
    public String getSlug(){
        return this.slug;
    }
    public void setWeight(String weight){
        this.weight = weight;
    }
    public String getWeight(){
        return this.weight;
    }
    public void setStatus(String status){
        this.status = status;
    }
    public String getStatus(){
        return this.status;
    }
    public void setLevel(String level){
        this.level = level;
    }
    public String getLevel(){
        return this.level;
    }
    public void setAvatar(String avatar){
        this.avatar = avatar;
    }
    public String getAvatar(){
        return this.avatar;
    }
    public void setUid(String uid){
        this.uid = uid;
    }
    public String getUid(){
        return this.uid;
    }

    public void setView(String view){
        this.view = view;
    }
    public String getView(){
        return this.view;
    }
    public void setCategory_slug(String category_slug){
        this.category_slug = category_slug;
    }
    public String getCategory_slug(){
        return this.category_slug;
    }
    public void setNick(String nick){
        this.nick = nick;
    }
    public String getNick(){
        return this.nick;
    }
    public void setBeauty_cover(String beauty_cover){
        this.beauty_cover = beauty_cover;
    }
    public String getBeauty_cover(){
        return this.beauty_cover;
    }
    public void setApp_shuffling_image(String app_shuffling_image){
        this.app_shuffling_image = app_shuffling_image;
    }
    public String getApp_shuffling_image(){
        return this.app_shuffling_image;
    }
    public void setFollow(int follow){
        this.follow = follow;
    }
    public int getFollow(){
        return this.follow;
    }

    public void setCategory_name(String category_name){
        this.category_name = category_name;
    }
    public String getCategory_name(){
        return this.category_name;
    }
    public void setThumb(String thumb){
        this.thumb = thumb;
    }
    public String getThumb(){
        return this.thumb;
    }
    public void setGrade(String grade){
        this.grade = grade;
    }
    public String getGrade(){
        return this.grade;
    }
    public void setHidden(boolean hidden){
        this.hidden = hidden;
    }
    public boolean getHidden(){
        return this.hidden;
    }
}
