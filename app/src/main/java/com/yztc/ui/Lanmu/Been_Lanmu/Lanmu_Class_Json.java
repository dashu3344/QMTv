package com.yztc.ui.Lanmu.Been_Lanmu;

import com.alibaba.fastjson.JSON;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/11/9.
 */

public class Lanmu_Class_Json {
    public static List<Lanmu_Class_Data> getData1(String json){
        List<Lanmu_Class_Data> lists= new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(json);
            JSONArray arr = jsonObject.getJSONArray("data");
            lists = JSON.parseArray(arr.toString(),Lanmu_Class_Data.class);
            return lists;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return lists;
    }
}
