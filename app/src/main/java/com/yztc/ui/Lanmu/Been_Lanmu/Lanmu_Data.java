package com.yztc.ui.Lanmu.Been_Lanmu;

/**
 * Created by Administrator on 2016/11/9.
 */

public class Lanmu_Data {

    private int id;
    private String name;
    private String slug;

    private String firstLetter;
    private int status;
    private int prompt;
    private String image;
    private String thumb;
    private int priority;
    private int screen;
    public void setId(int id) {
        this.id = id;
    }
    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }
    public String getSlug() {
        return slug;
    }

    public void setFirstLetter(String firstLetter) {
        this.firstLetter = firstLetter;
    }
    public String getFirstLetter() {
        return firstLetter;
    }

    public void setStatus(int status) {
        this.status = status;
    }
    public int getStatus() {
        return status;
    }

    public void setPrompt(int prompt) {
        this.prompt = prompt;
    }
    public int getPrompt() {
        return prompt;
    }

    public void setImage(String image) {
        this.image = image;
    }
    public String getImage() {
        return image;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }
    public String getThumb() {
        return thumb;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }
    public int getPriority() {
        return priority;
    }

    public void setScreen(int screen) {
        this.screen = screen;
    }
    public int getScreen() {
        return screen;
    }

}
