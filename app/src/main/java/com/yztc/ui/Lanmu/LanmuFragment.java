package com.yztc.ui.Lanmu;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.yztc.qmtv.LanmuActivity;
import com.yztc.qmtv.R;
import com.yztc.ui.Lanmu.Been_Lanmu.Lanmu_Data;
import com.yztc.ui.Lanmu.Been_Lanmu.MyGridView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class LanmuFragment extends Fragment {


    private String path = "http://www.quanmin.tv/json/categories/list.json?08121124&device=869515026912862&v=2.1.3&screen=2&ch=wandoujia&sh=1920&sw=1080&uid=1626943&net=0&ver=4&os=1";
    private Handler handler = new Handler();
    private List<Lanmu_Data> lists = new ArrayList<>();
    private MyGridViewAdapter adapter;
    private MyGridView gridView;
    private String path1;

    public LanmuFragment() {

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_lanmu,null);
        gridView = (MyGridView) view.findViewById(R.id.lanmu_GridView);
        //创建适配器
        adapter = new MyGridViewAdapter(getActivity(), lists);
        gridView.setAdapter(adapter);

        //获取栏目图标
        getData();
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String slug = lists.get(i).getSlug();
                path1 =  "http://www.quanmin.tv/json/categories/"+slug+"/list.json?uid=QO8PE69STWW4G6WO&screen=2&os=1&v=2.1.3&device=865168025985085&ch=OT_wdjcpd&ver=4&sh=1280&net=0&sw=720 ";
                Intent intent = new Intent(getActivity(), LanmuActivity.class);
                intent.putExtra("path1",path1);
                startActivity(intent);
            }
        });
        return view;
    }
    public void getData() {
        //给GridView准备数据
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    byte[] s = com.yztc.utils.HttpUtils.getJson(path);
                    String json = new String(s, 0,s.length);

                    lists.addAll(com.yztc.ui.Lanmu.Been_Lanmu.Lanmu_Json.getData(json));
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            adapter.notifyDataSetChanged();
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
