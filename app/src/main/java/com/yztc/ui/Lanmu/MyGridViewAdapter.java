package com.yztc.ui.Lanmu;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.yztc.qmtv.R;
import com.yztc.ui.Lanmu.Been_Lanmu.Lanmu_Data;

import java.util.List;

/**
 * Created by Administrator on 2016/11/8.
 */
public class MyGridViewAdapter extends BaseAdapter{
    private Context context;
    private List<Lanmu_Data> list;
    public  MyGridViewAdapter (Context context, List<Lanmu_Data> lists){
        this.context = context;
        this.list = lists;
    }
    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater=LayoutInflater.from(context);
        ViewHolder holder;
        if (view == null) {
            view = inflater.inflate(R.layout.lanmu_item,null);
            holder = new ViewHolder();

            holder.name = (TextView) view.findViewById(R.id.lanmu_name);
            holder.image = (ImageView) view.findViewById(R.id.lanmu_image);
            view.setTag(holder);
        }else {
            holder = (ViewHolder) view.getTag();
        }

        Lanmu_Data lanmu_item=list.get(i);
        String title = lanmu_item.getName();
        holder.name.setText(title);

        String imageUrl = lanmu_item.getImage();
        if (imageUrl != null) {
            //参数1上下文    参数2：图片路径  参数3：应用到Imageview
            Glide.with(context).load(imageUrl).into(holder.image);
        }else {
            holder.image.setImageResource(R.mipmap.ic_launcher);
        }
        return view;
    }

    public class ViewHolder {
        ImageView image;
        TextView name;

    }
}