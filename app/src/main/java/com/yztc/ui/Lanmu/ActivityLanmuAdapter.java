package com.yztc.ui.Lanmu;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.yztc.qmtv.R;
import com.yztc.ui.Lanmu.Been_Lanmu.Lanmu_Class_Data;

import java.util.List;

/**
 * Created by Administrator on 2016/11/9.
 */

public class ActivityLanmuAdapter extends BaseAdapter{
    private Context context;
    private List<Lanmu_Class_Data> lists;
    public ActivityLanmuAdapter (Context context ,List<Lanmu_Class_Data> lists ){
        this.context = context;
        this.lists = lists;
    }
    @Override
    public int getCount() {
        return lists.size();
    }

    @Override
    public Object getItem(int i) {
        return lists.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater=LayoutInflater.from(context);
        ViewHolder holder;
        if (view == null) {
            view = inflater.inflate(R.layout.lanmu_item_class,null);
            holder = new ViewHolder();

            holder.thumb = (ImageView) view.findViewById(R.id.item_class_image);
            holder.avatar = (ImageView) view.findViewById(R.id.item_class_image1);
            holder.nick = (TextView) view.findViewById(R.id.item_class_text);
            holder.title = (TextView) view.findViewById(R.id.item_class_text1);
            view.setTag(holder);
        }else {
            holder = (ViewHolder) view.getTag();
        }

        Lanmu_Class_Data lanmu_item=lists.get(i);
        String nick = lanmu_item.getNick();
        holder.nick.setText(nick);
        String title = lanmu_item.getTitle();
        holder.title.setText(title);
        String imageUrl = lanmu_item.getThumb();
        String ava = lanmu_item.getAvatar();

    if (imageUrl != null) {
        //参数1上下文    参数2：图片路径  参数3：应用到Imageview
        Glide.with(context).load(imageUrl).into(holder.thumb);
    }else {
        holder.thumb.setImageResource(R.mipmap.ic_launcher);
    }
        if (ava != null) {
            //参数1上下文    参数2：图片路径  参数3：应用到Imageview
            Glide.with(context).load(ava).into(holder.avatar);
        }else {
            holder.avatar.setImageResource(R.mipmap.ic_launcher);
        }
    return view;
}

    public class ViewHolder {
        ImageView thumb;
        ImageView avatar;
        TextView nick;
        TextView title;
    }

}
