package com.yztc.ui.Zhibo;

import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.yztc.adapter.ZhiboAdapter;
import com.yztc.bean.UserRoom;
import com.yztc.qmtv.R;
import com.yztc.qmtv.RoomActivity;
import com.yztc.utils.HttpUtils;
import com.yztc.utils.ParseUserRoom;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ZhiboFragment extends Fragment {
    private RecyclerView mRecyclerView;
    private ZhiboAdapter adapter;
    private List<UserRoom> lists;
    private String path = "http://www.quanmin.tv/json/play/list.json?11081718&os=1&v=2.2&ver=4";
    private ImageView iv_search,iv_thumb;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 1:
                    lists = (List<UserRoom>) msg.obj;
                    adapter = new ZhiboAdapter(getActivity(), lists);
                    mRecyclerView.setAdapter(adapter);
                    break;
                case 0:
                    break;
            }
        }
    };

    public ZhiboFragment() {
        // Required empty public constructor
        super();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_zhibo, container, false);
        initData();
        iv_search = (ImageView) view.findViewById(R.id.iv_search);
        iv_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "跳转到搜索界面", Toast.LENGTH_SHORT).show();
            }
        });
        iv_thumb = (ImageView) view.findViewById(R.id.iv_thumb);
        //点击缩略图进去到直播间  待实现
//        iv_thumb.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent();
//                intent.putExtra("category_id", "category_id");
//                intent.setClass(getActivity(),RoomActivity.class);
//                startActivity(intent);
//            }
//        });
        mRecyclerView = (RecyclerView) view.findViewById(R.id.rv);
        mRecyclerView.addItemDecoration(new SpacesItemDecoration(2));
        //如果确定每个item的内容不会改变RecyclerView的大小，设置这个选项可以提高性能
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        return view;
    }

    //得到数据集合
    protected void initData() {
        lists = new ArrayList<>();
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = Message.obtain();
                try {
                    byte[] b = HttpUtils.getJson(path);

                    if (b.length != 0) {
                        String jsonString = new String(b, 0, b.length);
                        if (!TextUtils.isEmpty(jsonString)) {
                            lists = ParseUserRoom.fromStringToList(jsonString);
                            Log.i("lists.size", "--------------" + lists.size());
                            if (lists.size() != 0 && lists != null) {
                                msg.what = 1;
                                msg.obj = lists;
                            } else {
                                msg.what = 0;
                                Log.i("tag", "数据为空，下载异常");
                            }
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                handler.sendMessage(msg);
            }
        }).start();
    }


    //增加item的间距
    private class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space;

        public SpacesItemDecoration(int space) {
            this.space = space;
        }

        //貌似没有什么卵用 先不管
        @Override
        public void getItemOffsets(Rect outRect, View view,
                                   RecyclerView parent, RecyclerView.State state) {
//            outRect.right = space;
//            outRect.bottom = space;
//            outRect.left = space;
            //只对第一行添加上边的间距
//            if (parent.getChildLayoutPosition(view) == 1) {
//                outRect.top = space;
//            }
            //入如果是左边列则绘制左边距
            if (parent.getChildLayoutPosition(view) % 2 == 0) {
                outRect.right = space;
            }
//            if (parent.getChildLayoutPosition(view) % 2 == 1){
//                outRect.left=space;
//            }
        }
    }
}
