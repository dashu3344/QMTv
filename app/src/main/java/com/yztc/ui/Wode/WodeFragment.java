package com.yztc.ui.Wode;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.yztc.qmtv.DengluActivity;
import com.yztc.qmtv.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class WodeFragment extends Fragment implements View.OnClickListener {
    private LinearLayout wo_denglu;
    private ImageView wo_iv_zuo;

    public WodeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_wode, container, false);

        //初始化数据
        initView(view);

        return view;
    }

    //初始化数据
    public void initView(View view) {
//        wo_denglu = (LinearLayout) view.findViewById(R.id.wo_denglu);
//         wo_iv_zuo= (ImageView) view.findViewById(R.id.wo_iv_zuo);

        wo_denglu.setOnClickListener(this);
        wo_iv_zuo.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
//            case R.id.wo_denglu:
//                Deng();
//                break;
//            case R.id.wo_iv_zuo:
//                Deng();
//                break;
        }
    }


    //登录跳转
    public void Deng(){
        Intent intent=new Intent(getActivity(),DengluActivity.class);
        startActivity(intent);
    }
}
