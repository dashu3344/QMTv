package com.yztc.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.yztc.bean.UserRoom;
import com.yztc.qmtv.R;

import java.text.DecimalFormat;
import java.util.List;

import static android.R.attr.publicKey;
import static android.R.attr.resource;

/**
 * Created by Administrator on 2016/11/8.
 */

public class ZhiboAdapter extends RecyclerView.Adapter<ZhiboAdapter.MyViewHolder> {
    private Context context;
    private List<UserRoom> lists;

    public ZhiboAdapter(Context context, List<UserRoom> lists) {
        this.context = context;
        this.lists = lists;
    }

    @Override
    public ZhiboAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        MyViewHolder holder = new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.item_zhibo, parent, false));
//        return holder;
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_zhibo, null));
    }

    @Override
    public void onBindViewHolder(ZhiboAdapter.MyViewHolder holder, int position) {
        holder.tv_nick.setText(lists.get(position).getNick());
        holder.tv_title.setText(lists.get(position).getTitle());
        //处理观看人数
        DecimalFormat df = new DecimalFormat("###.0");
        //背景图片半透明效果以及前面的白色小三角
        String view = lists.get(position).getView();
        holder.tv_view.setText(df.format(Float.parseFloat(view) / 10000) + "W");
        Log.i("aa", "----list.size---" + lists.size());

        String thumbPath = lists.get(position).getThumb();
        String avatarPath = lists.get(position).getAvatar();
        //圆角效果  暂未实现
//        Glide.with(context).load(thumbPath).asBitmap().into(holder.iv_thumb);
        Glide.with(context).load(thumbPath).transform(new GlideRoundTransform(context)).crossFade().into(holder.iv_thumb);
        //头像圆形
        Glide.with(context).load(avatarPath).transform(new CircleTransform(context)).into(holder.iv_avatar);

    }

    //实现房间缩略图的圆角效果
    public class GlideRoundTransform extends BitmapTransformation {
        private  float radius = 0f;

        public GlideRoundTransform(Context context) {
            this(context,4);
        }

        public GlideRoundTransform(Context context, int dp) {
            super(context);
            this.radius = Resources.getSystem().getDisplayMetrics().density * dp;
        }

        @Override
        protected Bitmap transform(BitmapPool pool, Bitmap toTransform, int outWidth, int outHeight) {
            return roundCrop(pool,toTransform);
        }

        private  Bitmap roundCrop(BitmapPool pool,Bitmap source){
            if (source==null) return null;

            Bitmap result=pool.get(source.getWidth(),source.getHeight(),Bitmap.Config.ARGB_8888);
            if (result==null){
                result=Bitmap.createBitmap(source.getWidth(),source.getHeight(),Bitmap.Config.ARGB_8888);
            }

            Canvas canvas=new Canvas(result);
            Paint paint=new Paint();
            paint.setShader(new BitmapShader(source,BitmapShader.TileMode.CLAMP,BitmapShader.TileMode.CLAMP));
            paint.setAntiAlias(true);
            RectF rectF=new RectF(0f,0f,source.getWidth(),source.getHeight());
            canvas.drawRoundRect(rectF,radius,radius,paint);
            return result;
        }
        @Override
        public String getId() {
            return getClass().getName()+Math.round(radius);
        }
    }

    //实现圆形图片的方法
    public static class CircleTransform extends BitmapTransformation {

        public CircleTransform(Context context) {
            super(context);
        }

        @Override
        protected Bitmap transform(BitmapPool pool, Bitmap toTransform, int outWidth, int outHeight) {
            return circleCrop(pool, toTransform);
        }

        private static Bitmap circleCrop(BitmapPool pool, Bitmap source) {
            if (source == null) return null;

            int size = Math.min(source.getWidth(), source.getHeight());
            int x = (source.getWidth() - size) / 2;
            int y = (source.getHeight() - size) / 2;

            //TODO this could be acquired from the pool too
            Bitmap squared = Bitmap.createBitmap(source, x, y, size, size);

            Bitmap result = pool.get(size, size, Bitmap.Config.ARGB_8888);
            if (result == null) {
                result = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888);
            }
            Canvas canvas = new Canvas(result);
            Paint paint = new Paint();
            paint.setShader(new BitmapShader(squared, BitmapShader.TileMode.CLAMP,
                    BitmapShader.TileMode.CLAMP));
            paint.setAntiAlias(true);
            float r = size / 2f;
            canvas.drawCircle(r, r, r, paint);
            return result;
        }

        @Override
        public String getId() {
            return getClass().getName();
        }
    }

    @Override
    public int getItemCount() {
        return lists.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_nick, tv_title, tv_view,tv_start;
        ImageView iv_avatar, iv_thumb;

        public MyViewHolder(View view) {
            super(view);
            tv_nick = (TextView) view.findViewById(R.id.tv_nick);
            tv_title = (TextView) view.findViewById(R.id.tv_title);
            tv_view = (TextView) view.findViewById(R.id.tv_view);
            //背景透明度
            tv_view.setBackgroundColor(Color.argb(120,120,120,120));
            tv_start= (TextView) view.findViewById(R.id.tv_start);
            tv_start.setBackgroundColor(Color.argb(120,120,120,120));
            iv_avatar = (ImageView) view.findViewById(R.id.iv_avatar);
            iv_thumb = (ImageView) view.findViewById(R.id.iv_thumb);
        }
    }
}
