package com.yztc.qmtv;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.widget.ScrollView;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshScrollView;
import com.yztc.ui.Lanmu.ActivityLanmuAdapter;
import com.yztc.ui.Lanmu.Been_Lanmu.Lanmu_Class_Data;
import com.yztc.ui.Lanmu.Been_Lanmu.MyGridView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class LanmuActivity extends AppCompatActivity {
    private MyGridView gridView;
    private ActivityLanmuAdapter  adapter;
    private Handler handler = new Handler();
    private List<Lanmu_Class_Data> lists=new ArrayList<>();
    private String path1;
    private PullToRefreshScrollView mPullToRefreshScrollView;
    private int IndexPage = 1;
    private boolean flag = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //隐藏标题栏
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_lanmu);

        mPullToRefreshScrollView = (PullToRefreshScrollView) findViewById(R.id.pull_refresh_scrollview);
        gridView = (MyGridView) findViewById(R.id.lanmu_GridView1);
        //创建适配器
        adapter = new ActivityLanmuAdapter(this, lists);
        gridView.setAdapter(adapter);
        Intent intent = getIntent();
        path1 = intent.getStringExtra("path1");
        getData();
        mPullToRefreshScrollView.setMode(PullToRefreshBase.Mode.BOTH);//设置刷新模式
        //添加监听事件
        mPullToRefreshScrollView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ScrollView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ScrollView> refreshView) {
                getData();
                IndexPage = 1;
                flag = true;

                mPullToRefreshScrollView.onRefreshComplete();
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ScrollView> refreshView) {
                IndexPage++;
                getData();
                mPullToRefreshScrollView.onRefreshComplete();
            }
        });
    }
    public void getData() {
        //给GridView准备数据
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    byte[] s = com.yztc.utils.HttpUtils.getJson(path1);
                    String json = new String(s, 0,s.length);
                    if (flag) {
                        lists.clear();
                        flag = false;
                    }
                    lists.addAll(com.yztc.ui.Lanmu.Been_Lanmu.Lanmu_Class_Json.getData1(json));
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            adapter.notifyDataSetChanged();
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
