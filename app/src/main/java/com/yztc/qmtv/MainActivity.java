package com.yztc.qmtv;


import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Window;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.yztc.ui.Lanmu.LanmuFragment;
import com.yztc.ui.Tuijian.TuiJianFragment;
import com.yztc.ui.Wode.WodeFragment;
import com.yztc.ui.Zhibo.ZhiboFragment;


public class MainActivity extends AppCompatActivity implements RadioGroup.OnCheckedChangeListener{
    //存放四个按钮的RadioGroup
    private RadioGroup rg;
    //四个Fragment
    private TuiJianFragment tuijianFragment;
    private LanmuFragment lanmuFragment;
    private ZhiboFragment zhiboFragment;
    private WodeFragment wodeFragment;
    private Long fristTime=(long)0;
    //fragment管理器
    FragmentManager Manager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        //隐藏标题栏
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        initview();
        initdata();
    }

    //初始化控件
    public void initview() {
        //初始化数据
        rg= (RadioGroup) findViewById(R.id.rg);
        rg.setOnCheckedChangeListener(this);
        Manager=getSupportFragmentManager();
    }



    public void initdata() {
        Setselection(0);
    }

    public void Setselection(int index) {
        FragmentTransaction ft=Manager.beginTransaction();
        hindFragment(ft);
        //需要加载的页面
        switch (index){
            case 0:
                if (tuijianFragment==null){
                    tuijianFragment=new TuiJianFragment();
                    //当快速双击调用FragmentTransaction.add()方法添加fragmentA，而fragmentA不是每次单独生成的，就会引起这个异常。
                   ft.add(R.id.fragment,tuijianFragment);
                }else{
                    ft.show(tuijianFragment);
                }
                break;
            case 1:

                if (lanmuFragment==null){
                    lanmuFragment=new LanmuFragment();
                    ft.add(R.id.fragment,lanmuFragment);
                }else{
                    ft.show(lanmuFragment);
                }
                break;
            case 2:

                if (zhiboFragment==null){
                    zhiboFragment=new ZhiboFragment();

                    ft.add(R.id.fragment,zhiboFragment);

                }else{
                    ft.show(zhiboFragment);
                }
                break;
            case 3:
                if (wodeFragment==null){
                    wodeFragment=new WodeFragment();
                    ft.add(R.id.fragment,wodeFragment);

                }else{

                    ft.show(wodeFragment);
                }
                break;
        }
        ft.commit();
    }

    private void hindFragment(FragmentTransaction ft) {
        //如果fragment不为空则保存当前fragment状态
        if (tuijianFragment!=null){
            ft.hide(tuijianFragment);
        }
        if (lanmuFragment!=null){
            ft.hide(lanmuFragment);
        }
        if (zhiboFragment!=null){
            ft.hide(zhiboFragment);
        }
        if (wodeFragment!=null){
            ft.hide(wodeFragment);
        }
    }
    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId){
            //推荐
            case R.id.rb_tuijian:
                Setselection(0);
                break;
            //书架
            case R.id.rb_lanmu:
                Setselection(1);
                Log.i("tag","-------------点击了............-------------------------------");
                break;
            //分类
            case R.id.rb_zhibo:
                Setselection(2);
                break;
            //更多
            case R.id.rb_wode:
                Setselection(3);
                break;
        }
    }

    //两次返回退出
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //退出窗口
        if (keyCode== KeyEvent.KEYCODE_BACK){
            if (System.currentTimeMillis()-fristTime>2000){
                Toast.makeText(MainActivity.this, "再按一次退出 程序", Toast.LENGTH_SHORT).show();
                fristTime=System.currentTimeMillis();
                return false;
            }else {
                finish();
                System.exit(keyCode);
            }
        }
        return super.onKeyDown(keyCode, event);
    }
}
